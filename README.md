###This is where I plan to keep all files and relevant information regarding my assistance in Professor Stephen's laboratory at UIC. This research mainly pertains to networks and systems, and includes theorizing and benchmarking primitives for programmable switches and NICs.

To read my daily log on research progress, please check the progress.md file.