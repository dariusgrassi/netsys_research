#Research Progress Log
 __This is where I will document all my progress regarding Professor Stephens research projects.__

##7/2/2019
- Cloned BESS codebase from github to surfer3 account
- Created an account on Komal's Ubuntu workspace w/ root privilege.
- Going to start building BESS on surfer3 account
- Installed newest version of Vagrant in BESS folder

##7/3/2019
- Ran into an error trying to install Vagrant plugins to run the Vagrantfile, currently trying to fix.
- Realized I was already ssh'd into an Ubuntu VM so I don't need Vagrant to make another one for me, attempting to build BESS inside surfer3 currently.
- Asked John about surfer3 and surfer4, learned that they are just servers and not virtual machines.
- Going to attempt to install virtualbox on my surfer3 account, then re-install vagrant and then try to build BESS inside of that VM.
- I have virtualbox's command line version installed as well as vagrant, but am unable to get vagrant onto my PATH and am trying to find its location.
- I'm stuck at trying to install Vagrant. When I run the Vagrant command, it tells me it couldn't find Vagrant and I need to use "sudo apt install vagrant". However, when I use this command, the console tells me that Vagrant 2.2.5 is already installed. Not sure what to do from here.

##7/5/2019
- Beginning Step 1 of research steps, iper3/sockperf benchmarking.
- Currently reads the exps and env README's.
- On step 1 of the README in loom/env. Stuck on testing `ansible all -i cluster-hosts -m ping`.
- Got past part 1 of Step 1, had to add --ask-pass onto the end of the above command.
- Now stuck on the next step which requires me to run `ansible-playbook -i cluster-hosts packages.yml`. When I run this I get some output but it seems to get stuck and the input line doesn't pop up again until I _ctrl-Z_.
- Heading out for the day. Still trying to figure out how to copy keys to surfer3 and surfer4 to enable passwordless SSH. Going to keep working on it over the weekend.

##7/8/2019
- I was able to set up passwordless ssh for surfer3 and surfer4 today. I'm now trying to install the packages needed for the benchmarks by running the ansible playbook packages.yml. Currently debugging to see what's going wrong.
- Couldn't get packages.yml to run. Assuming since the playbook is old, there may be something in the format that's causing it to break. Trying to skip step and get set up to run iperf3 and sockperf microbenchmarks.
- Pretty frustrating day. Only thing I was able to get working was passwordless SSH. My hypothesis is that the packages.yml file along with many of the scripts are broken or outdated. Going to take some time to get organized and then see if I can do a deep-dive to get some things running tomorrow.

##7/9/2019
- Met with Prof Stephens today. Went more in-depth on various topics and research I should be doing, as well as some more explanation and context for the microbenchmarks I should be able to replicate from the Loom research paper. 
- Spent most of the day reading various papers and articles.

##7/10/2019
- Picked up key for SEO 1300 from 11th floor. Key for 1336 doesn't work, a new one should be ready in ~2 weeks. 
- Cloned my bitbucket repo to surfer3 & 4, should be able to modify files to there. Gonna see what else I can accomplish with bitbucket today.
- Been tracking my notes and questions on the notes Google doc w/ Prof Stephens as well. Going to continue reading and researching as well today.
- I was able to update this progress file from command line to Bitbucket using Git. Should have a good foundation for Bitbucket from here on.
- Just set up an SSH key for Bitbucket. Writing this note to test if it's working correctly.
- Yep it works. Had to change url to ssh from https.

##7/11/2019
- Reading about TCP incast today. Going to try to transfer this repository to UIC Data Center Systems if I can figure it out. Also going to try to login to Ubuntu today and see if I can run Loom through that instead of MacOS.
